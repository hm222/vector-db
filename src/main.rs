use reqwest::Client;
use serde::{Deserialize, Serialize};
use plotters::prelude::*;

#[derive(Debug, Serialize, Deserialize)]
struct SearchPayload {
    vector: Vec<f64>,
    limit: i32,
    with_payload: bool,
    with_vector: bool
}

#[derive(Debug, Serialize, Deserialize)]
struct SearchResult {
    id: i32,
    version: i32,
    score: f64,
    payload: Payload,
    vector: Vec<f64>
}

#[derive(Debug, Serialize, Deserialize)]
struct Payload {
    tag: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct SearchResponse {
    result: Vec<SearchResult>,
    status: String,
    time: f64
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = Client::new();
    let search_payload = SearchPayload {
        vector: vec![0.1; 384],
        limit: 30,
        with_payload: true,
        with_vector: true
    };

    let response = client.post("http://localhost:6333/collections/rand_vec/points/search")
        .json(&search_payload)
        .send()
        .await?
        .json::<SearchResponse>()
        .await?;

    println!("The closest vector to all-zero vector is: {:?}", response.result[0].vector);

    println!("======================================================================");

    let green_count = response.result.iter().filter(|r| r.payload.tag == "green").count();
    let blue_count = response.result.iter().filter(|r| r.payload.tag == "blue").count();
    let red_count = response.result.iter().filter(|r| r.payload.tag == "red").count();

    println!("Tag Green: {}, Tag Blue: {}, Tag Red: {}", green_count, blue_count, red_count);

    let root = BitMapBackend::new("category_count.png", (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .caption("Color Counts", ("sans-serif", 50).into_font())
        .x_label_area_size(30)
        .y_label_area_size(40)
        .build_cartesian_2d(0..3, 0..std::cmp::max(green_count, std::cmp::max(blue_count, red_count)) + 10)?;

    chart.configure_mesh()
        .x_labels(3)
        .x_label_formatter(&|x| match *x {
            0 => "Green".to_string(),
            1 => "Blue".to_string(),
            2 => "Red".to_string(),
            _ => "".to_string(),
        })
        .draw()?;

    // Drawing bars for each color
    chart.draw_series(vec![
        Rectangle::new([(0, 0), (1, green_count)], GREEN.filled()),
        Rectangle::new([(1, 0), (2, blue_count)], BLUE.filled()),
        Rectangle::new([(2, 0), (3, red_count)], RED.filled()),
    ])?;

    root.present().expect("Unable to write result to file");
    println!("Visualization saved as category_counts.png");

    Ok(())
}
