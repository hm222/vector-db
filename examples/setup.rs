use reqwest::Client;
use serde::{Serialize, Deserialize};
use std::fs::File;
use std::io::BufReader;
use tokio;

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Payload {
    tag: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct Point {
    id: i64,  // Unique identifier for each vector
    payload: Payload,
    vector: Vec<f64>,
}

#[derive(Debug, Serialize, Deserialize)]
struct InsertRequest {
    points: Vec<Point>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Initialize the HTTP client
    let client = Client::new();

    // Adjust the path to your embeddings.json file as necessary
    let file = File::open("./examples/embeddings.json")
        .map_err(|e| e.to_string())?;
    let reader = BufReader::new(file);

    // Deserialize the JSON content into a Vec<Vec<f64>> representing the embeddings
    let embeddings: Vec<Vec<f64>> = serde_json::from_reader(reader)
        .map_err(|e| e.to_string())?;

    let tags = ["red", "blue", "green"];
    // Generating sequential IDs for each vector point and preparing the insert request
    let points: Vec<Point> = embeddings.into_iter().enumerate().map(|(index, vec)| Point {
        id: index as i64,  // Assuming sequential IDs are sufficient for your use case
        payload: Payload {tag: format!("{}", tags[index % 3])},
        vector: vec,
    }).collect();

    let insert_request = InsertRequest { points };

    // Sending the PUT request to Qdrant to insert the data
    let res = client.put("http://localhost:6333/collections/rand_vec/points")
        .json(&insert_request)
        .send()
        .await?;

    // Logging the response from Qdrant
    println!("Response status: {}", res.status());
    println!("Response body: {:?}", res.text().await?);

    Ok(())
}
