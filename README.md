# Vector Database Project with Qdrant, Python, and Rust

This project demonstrates an end-to-end workflow using Qdrant as a vector database to store and query high-dimensional vector data. The project encompasses creating a collection in Qdrant, generating vector data for sentences using Python, inserting and querying data with Rust, and visualizing the aggregation results.

## Overview

- **Vector Database**: [Qdrant](https://qdrant.tech/)
- **Language & Tools**: Rust, Python, and Curl
- **Key Features**: 
  - Creation of a vector data collection in Qdrant
  - Generation of embeddings for sentences using Python
  - Insertion of vector data into Qdrant using Rust
  - Querying and aggregating vector data based on payload information using Rust
  - Visualization of query results as a bar chart using Rust

## Setup and Installation

### Prerequisites

- Docker: For running the Qdrant container
- Rust: For data insertion, querying, and visualization code
- Python: For generating sentence embeddings

### Starting Qdrant with Docker

Run the following command to start a Qdrant container:

```sh
docker run -p 6333:6333 qdrant/qdrant
```

### Creating the Collection

Use the following `curl` command to create a collection in Qdrant (also saved as schema.sh):

```sh
curl -X PUT http://localhost:6333/collections/rand_vec \
-H 'Content-Type: application/json' \
--data-raw '{
    "vectors": {
      "size": 384,
      "distance": "Cosine"
    } 
  }'

```

### Generating Vector Data

The Python script `generate_embeddings.py` generates embeddings for 100 sentences. Ensure you have `sentence-transformers` installed:

```sh
pip install sentence-transformers
python vec_gen.py
```

### Inserting Data into Qdrant

Rust code in `examples/setup.rs` reads the generated embeddings and inserts them into the Qdrant collection. Run it with:

```sh
cargo run --example setup
```

### Querying and Aggregating Data

Rust code in `main.rs` performs queries against the vector data and aggregates results based on color tags in the payload. It also generates a bar chart visualization for the aggregation results. Execute it with:

```sh
cargo run
```

## Visualization

The project includes a simple Rust implementation for generating a bar chart that visualizes the count of vectors by their color tag in the payload. The output image `category_count.png` shows the distribution of vectors across different categories.

## Screenshots

- **Data Insertion Success**: ![Setup Screenshot](setup.png)
- **Query Success**: ![Query Screenshot](query.png)
- **Category Count Bar Chart**: ![Bar Chart](category_count.png)