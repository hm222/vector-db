import json
from sentence_transformers import SentenceTransformer

def main_function():
    model = SentenceTransformer('all-MiniLM-L6-v2')
    sentences = []
    for i in range(100):
        sentences.append(f"Sample Sentence {i}")
    embeddings = model.encode(sentences)
    
    # Export embeddings to a JSON file
    with open('embeddings.json', 'w') as f:
        json.dump(embeddings.tolist(), f)

if __name__ == "__main__":
    main_function()
